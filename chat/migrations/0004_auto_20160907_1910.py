# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django_unixdatetimefield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('chat', '0003_auto_20160907_1910'),
    ]

    operations = [
        migrations.AlterField(
            model_name='message',
            name='created',
            field=django_unixdatetimefield.fields.UnixDateTimeField(auto_now_add=True, null=True),
        ),
    ]
