from django.conf.urls import include, url
from django.contrib import admin
from chat import views

urlpatterns = [
    # Examples:
    # url(r'^$', 'intouch.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^$', views.home),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^send-message/', views.send_message),
    url(r'^get-message/', views.get_message),
    url(r'^get-user/', views.get_user),
]
