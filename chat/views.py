import json
import time

from django.shortcuts import render, get_object_or_404, render_to_response
from django.template import RequestContext, TemplateDoesNotExist
from django.http import HttpResponse, Http404
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User

from chat.models import Message
# Create your views here.

def home(request):
    return render_to_response("index.html",context_instance=RequestContext(request))


@login_required
def send_message(request):
    receiver = request.GET.get('id', '')
    message = request.GET.get('message', '')
    receiver = User.objects.get(id=receiver)
    new_message = Message.objects.create(text=message, receiver=receiver, sender=request.user)
    new_message.save()
    response = {'status':'created'}
    return HttpResponse(json.dumps(response))

@login_required
def get_message(request):
    sender = request.GET.get('id', '')
    message_after = request.GET.get('timestamp', '')
    sender = User.objects.get(id=sender)
    messages = Message.objects.filter(sender = sender, receiver=request.user)

    message_list = []
    for message in messages:
        print time.mktime(message.created.timetuple())
        if time.mktime(message.created.timetuple()) >=int(message_after):
            message_list.append(message.text)

    response = {'list': message_list}
    return HttpResponse(json.dumps(response))

@login_required
def get_user(request):
    user_list = User.objects.all().exclude(id=request.user.id)
    response = {'list': list(user_list.values_list('id','username'))}
    return HttpResponse(json.dumps(response))
    
