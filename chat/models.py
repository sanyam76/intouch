from django.db import models
from django.contrib.auth.models import User
from django_unixdatetimefield import UnixDateTimeField

# Create your models here.
class Message(models.Model):
    text = models.TextField()
    created = UnixDateTimeField(auto_now_add=True, null=True, blank=True)
    sender = models.ForeignKey(User, related_name='messages_sent')
    receiver = models.ForeignKey(User, related_name='messages_received')

    def __unicode__(self):
        return self.text
